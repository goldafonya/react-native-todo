export const THEME = {
  GREY_COLOR: "grey",
  DANGER_COLOR: "crimson",
  MAIN_COLOR: "#7d2181",
  paddingVertical: 30,
  paddingHorizontal: 20 
};
