import React, { useReducer, useContext } from "react";
import { TodoContext } from "./todoContext";
import { todoReducer } from "./todoReducer";
import {
  ADD_TODO,
  REMOVE_TODO,
  UPDATE_TODO,
  FETCH_TODOS,
  SHOW_LOADER,
  HIDE_LOADER,
  SHOW_ERROR,
  CLEAR_ERROR
} from "./typesContext";
import { ScreenContext } from "../screen/screenContext";

export const TodoState = ({ children }) => {
  const { changeScreen } = useContext(ScreenContext);
  const initialState = {
    todos: [],
    loading: false,
    error: null
  };

  const [state, dispatch] = useReducer(todoReducer, initialState);

  const addTodo = async title => {
    showLoader();
    clearError();
    try {
      const response = await fetch(
        "https://react-native-todo-acdff.firebaseio.com/todos.json",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({ title })
        }
      );
      const data = await response.json();
      dispatch({
        type: ADD_TODO,
        payload: { id: data.name, title }
      });
    } catch (e) {
      console.log(e);
      showError("Что-то пошло не так...");
    } finally {
      hideLoader();
    }
  };

  const fetchTodos = async () => {
    showLoader();
    clearError();
    try {
      const response = await fetch(
        "https://react-native-todo-acdff.firebaseio.com/todos.json",
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json"
          }
        }
      );
      const data = await response.json();
      const todos = Object.keys(data).map(key => ({
        id: key,
        title: data[key].title
      }));
      dispatch({
        type: FETCH_TODOS,
        payload: { todos }
      });
    } catch (e) {
      console.log(e);
      showError("Что-то пошло не так...");
    } finally {
      hideLoader();
    }
  };

  const removeTodo = async id => {
    const response = await fetch(
      `https://react-native-todo-acdff.firebaseio.com/todos/${id}.json`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json"
        }
      }
    );
    const data = await response.json();
    changeScreen(null);
    dispatch({
      type: REMOVE_TODO,
      payload: id
    });
  };

  const updateTodo = async (id, title) => {
    const response = await fetch(
      `https://react-native-todo-acdff.firebaseio.com/todos/${id}.json`,
      {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ title })
      }
    );
    const data = await response.json();
    dispatch({
      type: UPDATE_TODO,
      payload: { id, title }
    });
  };

  const showLoader = () => dispatch({ type: SHOW_LOADER });
  const hideLoader = () => dispatch({ type: HIDE_LOADER });
  const showError = error =>
    dispatch({
      type: SHOW_ERROR,
      payload: { error }
    });
  const clearError = () => dispatch({ type: CLEAR_ERROR });
  return (
    <TodoContext.Provider
      value={{
        todos: state.todos,
        loading: state.loading,
        error: state.error,
        addTodo,
        removeTodo,
        updateTodo,
        fetchTodos,
        showLoader,
        hideLoader,
        showError,
        clearError
      }}
    >
      {children}
    </TodoContext.Provider>
  );
};
