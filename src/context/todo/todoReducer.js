import {
  ADD_TODO,
  UPDATE_TODO,
  REMOVE_TODO,
  FETCH_TODOS,
  SHOW_LOADER,
  HIDE_LOADER,
  SHOW_ERROR,
  CLEAR_ERROR
} from "./typesContext";

export const todoReducer = (state, action) => {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        todos: [
          ...state.todos,
          {
            id: action.payload.id,
            title: action.payload.title
          }
        ]
      };
    case UPDATE_TODO:
      return {
        ...state,
        todos: state.todos.map(t => {
          if (t.id === action.payload.id) {
            t.title = action.payload.title;
          }
          return t;
        })
      };
    case REMOVE_TODO:
      return {
        ...state,
        todos: state.todos.filter(t => t.id !== action.payload)
      };
    case FETCH_TODOS:
      return {
        ...state,
        todos: action.payload.todos
      };
    case SHOW_LOADER:
      return {
        ...state,
        loading: true
      };
    case HIDE_LOADER:
      return {
        ...state,
        loading: false
      };
    case SHOW_ERROR:
      return {
        ...state,
        error: action.payload.error
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: null
      };
    default:
      return state;
  }
};
