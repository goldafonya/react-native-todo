import React, { useContext } from "react";
import { View, StyleSheet } from "react-native";
import { NavBar } from "./components/NavBar";
import { MainScreen } from "./screens/MainScreen";
import { ToDoScreen } from "./screens/ToDoScreen";
import { ScreenContext } from "./context/screen/screenContext";

export const MainLayout = () => {
  const { todoId } = useContext(ScreenContext);

  let content = !todoId ? <MainScreen /> : <ToDoScreen />;

  return (
    <View style={styles.wrapper}>
      <NavBar title={"ToDo App!"} />
      {content}
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1
  }
});
