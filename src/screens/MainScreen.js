import React, { useState, useEffect, useContext, useCallback } from "react";
import {
  View,
  FlatList,
  StyleSheet,
  Image,
  Text,
  Dimensions,
  Button
} from "react-native";
import { AddToDo } from "../components/AddToDo";
import { ToDo } from "../components/ToDo";
import { THEME } from "../theme";
import { TodoContext } from "../context/todo/todoContext";
import { ScreenContext } from "../context/screen/screenContext";
import { AppLoader } from "../components/AppLoader";

export const MainScreen = () => {
  const { todos, addTodo, removeTodo, fetchTodos, loading, error } = useContext(
    TodoContext
  );
  const { changeScreen } = useContext(ScreenContext);

  let content = null;
  const [deviceWidth, setDeviceWidth] = useState(
    Dimensions.get("window").width - THEME.paddingHorizontal * 2
  );

  const loadTodos = useCallback(async () => fetchTodos(), [fetchTodos]);

  useEffect(() => {
    loadTodos();
  }, []);
  useEffect(() => {
    const update = () =>
      setDeviceWidth(
        Dimensions.get("window").width - THEME.paddingHorizontal * 2
      );
    Dimensions.addEventListener("change", update);
    return () => Dimensions.removeEventListener("change", update);
  });
  if (error) {
    return (
      <View style={styles.center}>
        <Text style={styles.error}>{error}</Text>
        <Button title={"Повторить"} onPress={loadTodos} />
      </View>
    );
  }
  if (loading) {
    return <AppLoader />;
  }
  if (todos.length > 0) {
    content = (
      <View
        style={{
          width: deviceWidth
        }}
      >
        <FlatList
          data={todos}
          renderItem={({ item }) => (
            <ToDo
              title={item.title}
              id={item.id}
              onRemove={removeTodo}
              onOpen={changeScreen}
            />
          )}
          keyExtractor={item => item.id}
        />
      </View>
    );
  } else {
    content = (
      <View style={styles.imgWrap}>
        <Image
          style={styles.img}
          source={require("../../assets/Unknown.png")}
          resizeMode={"contain"}
        />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <AddToDo onSubmit={addTodo} />
      {content}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: THEME.paddingVertical,
    paddingHorizontal: THEME.paddingHorizontal
  },
  imgWrap: {
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
    height: 300
  },
  img: {
    width: "100%",
    height: "100%"
  },
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  error: {
    fontSize: 20,
    color: THEME.DANGER_COLOR
  }
});
