import React, { useState, useContext } from "react";
import { View, Text, StyleSheet, Dimensions, Alert } from "react-native";
import { THEME } from "../theme";
import { AppCard } from "../components/AppCard";
import { EditModal } from "../components/EditModal";
import { AppButton } from "../components/AppButton";
import { FontAwesome, AntDesign } from "@expo/vector-icons";
import { TodoContext } from "../context/todo/todoContext";
import { ScreenContext } from "../context/screen/screenContext";

export const ToDoScreen = () => {
  const [visible, setVisible] = useState(false);
  const { todos, removeTodo, updateTodo } = useContext(TodoContext);
  const { changeScreen, todoId } = useContext(ScreenContext);

  const goBack = () => changeScreen(null);
  const todo = todos.find(t => t.id === todoId);
  const onRemove = () => {
    Alert.alert(
      "Удаление элемента",
      `Вы уверены что хотите удалить "${todo.title}"?`,
      [
        {
          text: "Отмена",
          style: "cancel"
        },
        {
          text: "Удалить",
          style: "destructive",
          onPress: () => {
            removeTodo(todo.id);
          }
        }
      ],
      { cancelable: false }
    );
  };
  const updateTitle = async title => {
    await updateTodo(todoId, title);
    setVisible(false);
  };

  return (
    <View style={styles.container}>
      <EditModal
        value={todo.title}
        visible={visible}
        onClose={() => setVisible(false)}
        onSave={updateTitle}
      />
      <AppCard style={styles.card}>
        <Text style={styles.text}>{todo.title}</Text>
        <AppButton onPress={() => setVisible(true)}>
          <FontAwesome name={"edit"} size={20} />
        </AppButton>
      </AppCard>
      <View style={styles.buttons}>
        <View style={styles.button}>
          <AppButton
            style={styles.button}
            onPress={goBack}
            color={THEME.GREY_COLOR}
          >
            <AntDesign name={"back"} size={20} color={"#fff"} />
          </AppButton>
        </View>
        <View style={styles.button}>
          <AppButton
            style={styles.button}
            onPress={onRemove}
            color={THEME.DANGER_COLOR}
          >
            <FontAwesome name={"remove"} size={20} color={"#fff"} />
          </AppButton>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 30,
    paddingHorizontal: 20
  },
  buttons: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  button: {
    width: Dimensions.get("window").width > 400 ? 150 : 100
  },
  text: {
    fontSize: 18,
    fontWeight: "bold"
  },
  card: {
    marginBottom: 20,
    padding: 15
  }
});
