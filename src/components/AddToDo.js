import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Button,
  Alert,
  Keyboard
} from "react-native";
import { THEME } from "../theme";
import { AntDesign } from "@expo/vector-icons";

export const AddToDo = ({ onSubmit }) => {
  const [value, setValue] = useState("");

  const onPress = () => {
    if (value.trim()) {
      onSubmit(value);
      setValue("");
      Keyboard.dismiss();
    } else {
      Alert.alert("Название дела не может быть пустым!");
    }
  };

  return (
    <View style={styles.view}>
      <TextInput
        style={styles.input}
        value={value}
        onChangeText={setValue}
        placeholder={"Введите название дела"}
        autoCorrect={false}
        autoCapitalize={"none"}
      />
      <AntDesign.Button name={"pluscircleo"} onPress={onPress}>
        Добавить
      </AntDesign.Button>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    backgroundColor: "#fff",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around"
  },
  input: {
    borderBottomColor: THEME.MAIN_COLOR,
    borderBottomWidth: 2,
    flex: 0.7,
    fontSize: 16
  },
  button: {}
});
