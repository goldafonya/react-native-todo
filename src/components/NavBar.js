import React from "react";
import { View, StyleSheet, Text, Platform } from "react-native";
import { THEME } from "../theme";

export const NavBar = props => {
  return (
    <View
      style={{
        ...styles.container,
        ...Platform.select({
          ios: styles.navbarIOS,
          android: styles.navbarAndroid
        })
      }}
    >
      <Text style={styles.text} onPress={() => console.log("onPress NavBar")}>
        {props.title}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 80,
    paddingTop: 40,
    alignItems: "center"
  },
  navbarAndroid: {
    backgroundColor: THEME.MAIN_COLOR
  },
  navbarIOS: {
    borderBottomWidth: THEME.MAIN_COLOR,
    borderBottomWidth: 1
  },
  text: {
    color: Platform.OS === "ios" ? THEME.MAIN_COLOR : "#fff",
    fontSize: 22,
    fontWeight: "bold"
  }
});
